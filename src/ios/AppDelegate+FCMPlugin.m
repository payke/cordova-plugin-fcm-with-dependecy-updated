//
//  AppDelegate+FCMPlugin.m
//  TestApp
//
//  Created by felipe on 12/06/16.
//
//
#import "AppDelegate+FCMPlugin.h"
#import "FCMPlugin.h"
#import <objc/runtime.h>
#import <Foundation/Foundation.h>

#import "Firebase.h"

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@import UserNotifications;
#endif

@import FirebaseInstanceID;
@import FirebaseMessaging;

// Implement UNUserNotificationCenterDelegate to receive display notification via APNS for devices
// running iOS 10 and above. Implement FIRMessagingDelegate to receive data message via FCM for
// devices running iOS 10 and above.
typedef void (^CompletionHandlerType)();

#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
@interface AppDelegate () <UNUserNotificationCenterDelegate, FIRMessagingDelegate, NSURLSessionDelegate>
@property NSMutableDictionary *completionHandlerDictionary;
@end
#endif

// Copied from Apple's header in case it is missing in some cases (e.g. pre-Xcode 8 builds).
#ifndef NSFoundationVersionNumber_iOS_9_x_Max
#define NSFoundationVersionNumber_iOS_9_x_Max 1299
#endif

@implementation AppDelegate (MCPlugin)

static NSData *lastPush;
NSString *const kGCMMessageIDKey = @"gcm.message_id";

//Method swizzling
+ (void)load
{
    Method original =  class_getInstanceMethod(self, @selector(application:didFinishLaunchingWithOptions:));
    Method custom =    class_getInstanceMethod(self, @selector(application:customDidFinishLaunchingWithOptions:));
    method_exchangeImplementations(original, custom);
}

- (BOOL)application:(UIApplication *)application customDidFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [self application:application customDidFinishLaunchingWithOptions:launchOptions];
    
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    [self googleAnalyticsWithUserInfo:userInfo handler:nil eventname:@"_killed"];

    NSLog(@"DidFinishLaunchingWithOptions");
    // [START configure_firebase]
    [FIRApp configure];
    // [END configure_firebase]
    
    // Register for remote notifications. This shows a permission dialog on first run, to
    // show the dialog at a more appropriate time move this registration accordingly.
    if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_7_1) {
        // iOS 7.1 or earlier. Disable the deprecation warnings.
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wdeprecated-declarations"
        UIRemoteNotificationType allNotificationTypes =
        (UIRemoteNotificationTypeSound |
         UIRemoteNotificationTypeAlert |
         UIRemoteNotificationTypeBadge);
        [application registerForRemoteNotificationTypes:allNotificationTypes];
#pragma clang diagnostic pop
    } else {
        // iOS 8 or later
        // [START register_for_notifications]
        if (floor(NSFoundationVersionNumber) <= NSFoundationVersionNumber_iOS_9_x_Max) {
            UIUserNotificationType allNotificationTypes =
            (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
            UIUserNotificationSettings *settings =
            [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
            [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
        } else {
            // iOS 10 or later
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
            UNAuthorizationOptions authOptions =
            UNAuthorizationOptionAlert
            | UNAuthorizationOptionSound
            | UNAuthorizationOptionBadge;
            [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
            }];
            
            // For iOS 10 display notification (sent via APNS)
            [UNUserNotificationCenter currentNotificationCenter].delegate = self;
            // For iOS 10 data message (sent via FCM)
            [FIRMessaging messaging].delegate = self;
#endif
        }
        
        [[UIApplication sharedApplication] registerForRemoteNotifications];
        // [END register_for_notifications]
    }
    // Add observer for InstanceID token refresh callback.
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(tokenRefreshNotification:)
                                                 name:kFIRInstanceIDTokenRefreshNotification object:nil];
    return YES;
}

// [START message_handling]
// Receive displayed notifications for iOS 10 devices.

// Note on the pragma: When compiling with iOS 10 SDK, include methods that
//                     handle notifications using notification center.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0

// Handle incoming notification messages while app is in the foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
       willPresentNotification:(UNNotification *)notification
         withCompletionHandler:(void (^)(UNNotificationPresentationOptions))completionHandler {
    // Print message ID.
    NSDictionary *userInfo = notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID 1: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"%@", userInfo);
    
    NSError *error;
    NSDictionary *userInfoMutable = [userInfo mutableCopy];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfoMutable
                                                       options:0
                                                         error:&error];
    
    [FCMPlugin.fcmPlugin notifyOfMessage:jsonData];
    
    // Change this to your preferred presentation option
    completionHandler(UNNotificationPresentationOptionNone);
}

// Handle notification messages after display notification is tapped by the user.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center
didReceiveNotificationResponse:(UNNotificationResponse *)response
         withCompletionHandler:(void (^)())completionHandler {
    NSDictionary *userInfo = response.notification.request.content.userInfo;
    if (userInfo[kGCMMessageIDKey]) {
        NSLog(@"Message ID 2: %@", userInfo[kGCMMessageIDKey]);
    }
    
    // Print full message.
    NSLog(@"aaa%@", userInfo);
    
    NSError *error;
    NSDictionary *userInfoMutable = [userInfo mutableCopy];
    

    NSLog(@"New method with push callback: %@", userInfo);
    
    [userInfoMutable setValue:@(YES) forKey:@"wasTapped"];
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfoMutable
                                                        options:0
                                                            error:&error];
    NSLog(@"APP WAS CLOSED DURING PUSH RECEPTION Saved data: %@", jsonData);
    lastPush = jsonData;

    
    completionHandler();
}
#endif

// [START receive_message in background iOS < 10]

// Include the iOS < 10 methods for handling notifications for when running on iOS < 10.
// As in, even if you compile with iOS 10 SDK, when running on iOS 9 the only way to get
// notifications is the didReceiveRemoteNotification.

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
{
    // Short-circuit when actually running iOS 10+, let notification centre methods handle the notification.
    if (NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_9_x_Max) {
        return;
    }

    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);
    
    NSError *error;
    NSDictionary *userInfoMutable = [userInfo mutableCopy];
    
    if (application.applicationState != UIApplicationStateActive) {
        NSLog(@"New method with push callback: %@", userInfo);
        
        [userInfoMutable setValue:@(YES) forKey:@"wasTapped"];
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfoMutable
                                                           options:0
                                                             error:&error];
        NSLog(@"APP WAS CLOSED DURING PUSH RECEPTION Saved data: %@", jsonData);
        lastPush = jsonData;
    }
}
// [END receive_message in background] iOS < 10]

// [START receive_message iOS < 10]
- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo
fetchCompletionHandler:(void (^)(UIBackgroundFetchResult))completionHandler
{
    // Short-circuit when actually running iOS 10+, let notification centre methods handle the notification.
    @try {
        [self paykeReceiveNotificationEventRegisterWithUserInfo:userInfo];
    }
    @catch (NSException *e){
        NSLog(@"[Payke]ReceiveNotificationEvent faile. reason = %@",e);
    }
    [self googleAnalyticsWithUserInfo:userInfo handler:completionHandler eventname:@""];
    if (NSFoundationVersionNumber >= NSFoundationVersionNumber_iOS_9_x_Max) {
        return;
    }
    // If you are receiving a notification message while your app is in the background,
    // this callback will not be fired till the user taps on the notification launching the application.
    // TODO: Handle data of notification

    // Print message ID.
    NSLog(@"Message ID: %@", userInfo[@"gcm.message_id"]);

    // Pring full message.
    NSLog(@"%@", userInfo);
    NSError *error;

    NSDictionary *userInfoMutable = [userInfo mutableCopy];

    // Has user tapped the notificaiton?
    // UIApplicationStateActive   - app is currently active
    // UIApplicationStateInactive - app is transitioning from background to
    //                              foreground (user taps notification)

    if (application.applicationState == UIApplicationStateActive
        || application.applicationState == UIApplicationStateInactive) {
        [userInfoMutable setValue:@(NO) forKey:@"wasTapped"];
        NSLog(@"app active");
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:userInfoMutable
                                                           options:0
                                                             error:&error];
        [FCMPlugin.fcmPlugin notifyOfMessage:jsonData];

    // app is in background
    }

    completionHandler(UIBackgroundFetchResultNoData);
}
// [END receive_message iOS < 10]
// [END message_handling]


// [START refresh_token]
- (void)tokenRefreshNotification:(NSNotification *)notification
{
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result,
                                                        NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Error fetching remote instance ID: %@", error);
        } else {
            NSLog(@"Remote instance ID token: %@", result.token);
            [FCMPlugin.fcmPlugin notifyOfTokenRefresh:result.token];
            [self connectToFcm];
        }
    }];
}
// [END refresh_token]

- (void)googleAnalyticsWithUserInfo:(NSDictionary *)userInfo handler:(void (^)(UIBackgroundFetchResult))completionHandler eventname:(NSString *)eventname
{
    NSLog(@"START googleAnalytics");
    // 取得するコンテンツの URL
    if(eventname == nil){
      eventname = @"";
    }

    NSString *tid = @"";
    if([[userInfo allKeys] containsObject:@"tid"]){
      tid = [userInfo objectForKey:@"tid"];
    }

    NSString *body = @"";
    if([[userInfo allKeys] containsObject:@"body"]){
      body = [userInfo objectForKey:@"body"];
    }

    NSString *url_str = [
      NSString stringWithFormat:
        @"https://www.google-analytics.com/r/collect?v=1&t=event&ec=Push_Notification_Received%@&tid=%@&ev=100&cid=555&ea=%@&el=ios&z=%d",
        eventname,
        tid,
        [body stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet alphanumericCharacterSet]],
        (int)arc4random_uniform(10000000000)
    ];
    NSURL *url = [NSURL URLWithString: url_str];
    NSLog(@"GET Request to %@", url_str);

    // セッションの種類を決めます
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];

    // HTTPリクエスト
    NSURLSessionConfiguration *sessionConfiguration = [NSURLSessionConfiguration defaultSessionConfiguration];

    // サブスレッドで実行する
    NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfiguration];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request
        completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
            // ここにエラー処理などを書く
            // セッションの後始末
            [session invalidateAndCancel];
            if(completionHandler != nil){
                completionHandler(UIBackgroundFetchResultNoData);
            }
    }];
    [task resume];
    NSLog(@"END googleAnalytics");
    return;
}

- (void)paykeReceiveNotificationEventRegisterWithUserInfo:(NSDictionary *)userInfo
{
    NSLog(@"[Payke]Push_Notification_Receive_Event register. Start");
    NSString *queue_token     = [userInfo valueForKey:@"token"];
    NSString *push_history_id = [userInfo valueForKey:@"pushHistoryId"];
    NSString *queue_uri = [[NSBundle mainBundle] objectForInfoDictionaryKey:@"PAYKE_QUEUE_URI"];
    if (queue_token == nil || push_history_id == nil || queue_uri == nil) {
        NSLog(@"[Payke]Push_Notification_Receive_Event register. Skip");
        return;
    }
    /* QueueStorageへの登録 */
    [[FIRInstanceID instanceID] instanceIDWithHandler:^(FIRInstanceIDResult * _Nullable result, NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"[Payke]Push_Notification_Receive_Event register. undefined instance ID: %@", error);
            return;
        } else {
            NSLog(@"[Payke]Push_Notification_Receive_Event register. create request");
            NSURL    *url = [NSURL URLWithString: [NSString stringWithFormat:@"%@/messages?%@", queue_uri, queue_token]];
            NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url];
            [request setHTTPMethod:@"POST"];
            [request setValue:@"text/xml" forHTTPHeaderField:@"Content-Type"];
            [request setValue:@"text/xml" forHTTPHeaderField:@"Accept"];
            [request setHTTPBody:[self getToQueueMessageXmlDataWithPushId:push_history_id fcm_token:result.token]];
            NSLog(@"[Payke]Push_Notification_Receive_Event register. create request comp.");
            
            NSLog(@"[Payke]Push_Notification_Receive_Event register. create session");
            NSURLSessionConfiguration *sessionConfig;
            sessionConfig = [NSURLSessionConfiguration backgroundSessionConfigurationWithIdentifier:push_history_id];
            sessionConfig.allowsCellularAccess = YES;
            NSURLSession *session = [NSURLSession sessionWithConfiguration:sessionConfig
                                                                  delegate:self
                                                             delegateQueue:[NSOperationQueue mainQueue]];
            NSURLSessionDownloadTask *task = [session downloadTaskWithRequest:request];
            NSLog(@"[Payke]Push_Notification_Receive_Event register. session resume");
            [task resume];
            NSLog(@"[Payke]Push_Notification_Receive_Event register. End");
        }
    }];
    return;
}

- (NSData *)getToQueueMessageXmlDataWithPushId:(NSString *)push_history_id fcm_token:(NSString *)fcm_token
{
    /* QueueStorageのメッセージとして格納する受信したPush通知に関する情報を生成 */
    UIApplicationState state = [UIApplication sharedApplication].applicationState;
    NSDictionary     *params = @{
        @"fcm_token": fcm_token,
        @"notification_history_id": push_history_id,
        @"application_state": state == UIApplicationStateBackground ? @"1":@"2"
    };
    NSError  *error    = nil;
    NSData   *data     = [NSJSONSerialization dataWithJSONObject:params options:2 error:&error];
    NSString *json_str = [[NSString alloc]initWithData:data encoding:NSUTF8StringEncoding];
    NSString *body_xml = [NSString stringWithFormat:@"<QueueMessage><MessageText>%@</MessageText></QueueMessage>", json_str];
    return [body_xml dataUsingEncoding:NSUTF8StringEncoding];
}

- (void)application:(UIApplication *)application handleEventsForBackgroundURLSession:(NSString *)identifier completionHandler:(void (^)())completionHandler
{
    [self addCompletionHandler:completionHandler forSession:identifier];
}

#pragma mark - NSURLSessionDelegate
- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    if (session.configuration.identifier) {
        [self callCompletionHandlerForSession:session.configuration.identifier];
    }
}

#pragma mark - Private methods
- (void)addCompletionHandler:(CompletionHandlerType)handler forSession:(NSString *)identifier
{
    self.completionHandlerDictionary[identifier] = handler;
}

- (void)callCompletionHandlerForSession: (NSString *)identifier
{
    CompletionHandlerType handler = self.completionHandlerDictionary[identifier];
    if (handler) {
        [self.completionHandlerDictionary removeObjectForKey:identifier];
        handler();
    }
}

// [START connect_to_fcm]
- (void)connectToFcm
{   
    // Won't connect since there is no token
    if (![[FIRInstanceID instanceID] token]) {
        return;
    }
    
    // Disconnect previous FCM connection if it exists.
    [[FIRMessaging messaging] disconnect];
    
    [[FIRMessaging messaging] connectWithCompletion:^(NSError * _Nullable error) {
        if (error != nil) {
            NSLog(@"Unable to connect to FCM. %@", error);
        } else {
            NSLog(@"Connected to FCM.");
            [[FIRMessaging messaging] subscribeToTopic:@"ios"];
            [[FIRMessaging messaging] subscribeToTopic:@"all"];
        }
    }];
}
// [END connect_to_fcm]

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    NSLog(@"app become active");
    [FCMPlugin.fcmPlugin appEnterForeground];
    [self connectToFcm];
}

// [START disconnect_from_fcm]
- (void)applicationDidEnterBackground:(UIApplication *)application
{
    NSLog(@"app entered background");
    [[FIRMessaging messaging] disconnect];
    [FCMPlugin.fcmPlugin appEnterBackground];
    NSLog(@"Disconnected from FCM");
}
// [END disconnect_from_fcm]

+(NSData*)getLastPush
{
    NSData* returnValue = lastPush;
    lastPush = nil;
    return returnValue;
}


@end
