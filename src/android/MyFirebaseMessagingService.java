package com.gae.scaffolder.plugin;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import java.util.Map;
import java.util.HashMap;

import android.app.Notification;
import android.app.NotificationChannel;
import android.os.Build;
import android.content.pm.PackageManager;
import android.content.pm.ApplicationInfo;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import com.payke.ga.plugin.MyGoogleAnalytics;
import com.payke.ga.plugin.MyNotificationAnalytics;

/**
 * Created by Felipe Echanique on 08/06/2016.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "FCMPlugin";
    private MyGoogleAnalytics mga;
    private MyNotificationAnalytics mna;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        try {
            // TODO(developer): Handle FCM messages here.
            // If the application is in the foreground handle both data and notification messages here.
            // Also if you intend on generating your own notifications as a result of a received FCM
            // message, here is where that should be initiated. See sendNotification method below.
            Log.d(TAG, "==> MyFirebaseMessagingService onMessageReceived");
            
            if( remoteMessage.getNotification() != null){
                Log.d(TAG, "\tNotification Title: " + remoteMessage.getNotification().getTitle());
                Log.d(TAG, "\tNotification Message: " + remoteMessage.getNotification().getBody());
            }
            
            Map<String, Object> data = new HashMap<String, Object>();
            for (String key : remoteMessage.getData().keySet()) {
                    Object value = remoteMessage.getData().get(key);
                    Log.d(TAG, "\tKey: " + key + " Value: " + value);
                    data.put(key, value);
            }
            if (!data.containsKey("wasTapped")) {
                data.put("wasTapped", false);
            }
            Log.d(TAG, "\tNotification Data: " + data.toString());
            FCMPlugin.sendPushPayload( data );

            sendNotification(data.get("title").toString(), data.get("body").toString(), data);
            registerPaykeNotificationReceiveEvent(data);
            mga = new MyGoogleAnalytics();
            mga.ga(data);
        }
        catch(Exception e) {
            Log.e(TAG, "\tNotification Error: " + e.getMessage());
        }
    }
    // [END receive_message]

    @Override
    public void onNewToken(String refreshedToken) {
        // Get updated InstanceID token.
        //String refreshedToken = FirebaseInstanceId.getInstance().getToken();

        Log.d(TAG, "Refreshed token(onNewToken): " + refreshedToken);
        FCMPlugin.sendTokenRefresh( refreshedToken );
        // TODO: Implement this method to send any registration to your app's servers.
        //sendRegistrationToServer(refreshedToken);
    }
    
    // アプリの表示名取得
    public String getAppLable(Context context) {
        PackageManager packageManager = context.getPackageManager();
        ApplicationInfo applicationInfo = null;
        try {
            applicationInfo = packageManager.getApplicationInfo(context.getApplicationInfo().packageName, 0);
        } catch (PackageManager.NameNotFoundException e) {
        }
        return (String) (applicationInfo != null ? packageManager.getApplicationLabel(applicationInfo) : "Unknown");
    }

    private String getChannelId() {
        return getPackageName();
    }

    private void createNotificationChannel() {
        // Android8.0(API Level:26)以上の場合、通知を格納するためのチャンネルを作成する
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            // TODO: 当該対応を行わないとシステムトレイ(通知バー)に通知が格納できないため対応優先で実装。通知の種類別にチャンネル作成等はしていない
            CharSequence channel_name  = getAppLable(this); // 端末の設定画面上で表示される通知のグループ名。アプリの表示名で固定
            String channel_description = ""; // 通知チャンネルの概要説明欄。雑多な通知が格納されるため未入力
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel(getChannelId(), channel_name, importance);
            channel.setDescription(channel_description);
            channel.setLockscreenVisibility(Notification.VISIBILITY_PRIVATE);

            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private void sendNotification(String title, String messageBody, Map<String, Object> data) {
        createNotificationChannel();
        Intent intent = new Intent(this, FCMPluginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        data.put("wasTapped", true);
		for (String key : data.keySet()) {
			intent.putExtra(key, data.get(key).toString());
		}
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);
        int resID = getResources().getIdentifier("fcm_push_icon" , "mipmap", getPackageName());
        if(resID == 0){
            resID = getApplicationInfo().icon;
        }
        Uri defaultSoundUri= RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(resID)
                .setContentTitle(title)
                .setContentText(messageBody)
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            notificationBuilder.setChannelId(getChannelId());
        }
        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        int notificationId = 0;
        if (data.containsKey("pushHistoryId")) {
            notificationId = Integer.parseInt(data.get("pushHistoryId").toString());
        }
        notificationManager.notify(notificationId /* ID of notification */, notificationBuilder.build());
    }

    private void registerPaykeNotificationReceiveEvent(Map<String, Object> payload) {
		Log.d(TAG, "==> FCMPlugin registerPaykeNotificationReceiveEvent");
		try {
			mna = new MyNotificationAnalytics();
			mna.register(this, payload);
		} catch (Exception e) {
			Log.d(TAG, "\tERROR registerPaykeNotificationReceiveEvent: " + e.getMessage());
		}
	}
}
