package com.payke.ga.plugin;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.net.Uri;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.HashMap;
import java.util.Set;
import java.util.Random;
import java.util.Map;



public class MyGoogleAnalytics {//extends AsyncTask<String, Void, Bitmap> {

    private static final String TAG = "FCMPlugin";
    // // 途中経過をメインスレッドに返す
    // @Override
    // protected void onProgressUpdate(Void... progress) {
    //     //working cursor を表示させるようにしてもいいでしょう
    // }

    // void setListener(Listener listener) {
    //     this.listener = listener;
    // }

    // interface Listener {
    //     void onSuccess();
    // }

    public void ga(Map<String, Object> data) {
        final StringBuilder result = new StringBuilder();
        HttpURLConnection urlConnection = null;
        String urlString = "https://www.google-analytics.com/collect";
        HashMap<String, String> queryParams = new HashMap<String, String>();
        Random r = new Random();
        queryParams.put("v", "1");
        // GAの送信先は取得する
        Object obj_tid = data.get("tid");
        String tid = "";
        if(obj_tid != null){
          tid = obj_tid.toString();
        }
        Object obj_body = data.get("body");
        String body = "";
        if(obj_body != null){
          body = obj_body.toString();
        }

        queryParams.put("tid", tid);
        queryParams.put("cid", "555");
        queryParams.put("t", "event");
        queryParams.put("ec", "Push_Notification_Received");
        queryParams.put("ea", body);
        queryParams.put("el", "android");
        queryParams.put("ev", "100");
        queryParams.put("z", String.valueOf(r.nextInt(100000)));

        try {
            Uri.Builder builder = new Uri.Builder();
            for (String key : queryParams.keySet()) {
                builder.appendQueryParameter(key, queryParams.get(key));
            }
            URL url = new URL(urlString + builder.toString());

            Log.i(TAG, urlString + builder.toString());

            // HttpURLConnection インスタンス生成
            urlConnection = (HttpURLConnection) url.openConnection();

            // タイムアウト設定
            urlConnection.setReadTimeout(10000);
            urlConnection.setConnectTimeout(20000);

            // リクエストメソッド
            urlConnection.setRequestMethod("GET");

            // リダイレクトを自動で許可しない設定
            urlConnection.setInstanceFollowRedirects(false);

            // ヘッダーの設定(複数設定可能)
            urlConnection.setRequestProperty("Accept-Language", "jp");

            // 接続
            urlConnection.connect();

            int resp = urlConnection.getResponseCode();

            switch (resp){
                case HttpURLConnection.HTTP_OK:
                    Log.i(TAG, "HTTP_OK");
                    InputStream is = null;
                    try{
                        is = urlConnection.getInputStream();
                        is.close();
                    } catch(IOException e){
                        e.printStackTrace();
                    } finally{
                        if(is != null){
                            is.close();
                        }
                    }
                    break;
                case HttpURLConnection.HTTP_UNAUTHORIZED:
                    Log.i(TAG, "HTTP_UNAUTHORIZED");
                    break;
                default:
                    Log.i(TAG, "HTTP_OTHER");
                    break;
            }
        } catch (Exception e) {
            Log.i(TAG, "Exception");
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
        }
        return;
    }
}
